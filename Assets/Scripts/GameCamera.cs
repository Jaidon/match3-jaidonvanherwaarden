﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    Board boardScript;
    void Start()
    {
        boardScript = FindObjectOfType<Board>();

        float y = Mathf.Max((boardScript.boardLength)*(1920.0f/1080.0f), boardScript.boardWidth);

        Vector3 position = new Vector3(0.0f, y, 0.0f);

        transform.position = position;
        transform.rotation = Quaternion.LookRotation(Vector3.down);
    }
}