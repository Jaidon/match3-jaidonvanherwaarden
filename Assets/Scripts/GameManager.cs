﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject userInterface;

    public int matchesToWin;
    public int matchesLeftToWin;

    public int score = 0;

    public bool isGameOver = false;

    private void Awake()
    {
        matchesLeftToWin = matchesToWin;
        userInterface.SetActive(false);
    }
    public void Score()
    {
        score++;
    }
    public void MakeMatch()
    {
        matchesLeftToWin--;
        if(matchesLeftToWin <= 0)
        {
            GameOver();
        }
    }
    public void GameOver()
    {
        userInterface.SetActive(true);
        Time.timeScale = 0.0f;
        isGameOver = true;

        userInterface.GetComponentInChildren<Text>().text += score;
    }
    public void PlayAgain()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    public void Quit()
    {
        Application.Quit();
    }
}