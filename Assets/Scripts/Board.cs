﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    GameManager gameManagerScript;

    public int boardWidth = 10;
    public int boardLength = 10;

    public GameObject tile;

    GameObject[] tileArray;

    List<int> firstTypeDeadTiles = new List<int>();
    List<int> secondTypeDeadTiles = new List<int>();

    Tile firstTileToSwap = null;
    Tile secondTileToSwap = null;

    private void Awake()
    {
        gameManagerScript = FindObjectOfType<GameManager>();

        BuildBoard();
    }

    public void BuildBoard()
    {
        if (boardLength < 4)
        {
            boardLength = 4;
        }
        else if(boardLength > 20)
        {
            boardLength = 20;
        }
        if (boardWidth < 4)
        {
            boardWidth = 4;
        }
        else if(boardWidth > 20)
        {
            boardWidth = 20;
        }

        tileArray = new GameObject[boardWidth * boardLength];
        int tileIndex = 0;

        for (int length = 0; length < boardLength; ++length)
        {
            for (int width = 0; width < boardWidth; ++width)
            {
                float x = width - ((boardWidth-1.0f) / 2.0f);
                float z = length - ((boardLength - 1.0f) / 2.0f);

                Vector3 position = new Vector3(x, 0.0f, z);

                tileArray[tileIndex] = Instantiate(tile, position, Quaternion.identity);
                tileArray[tileIndex].name = "Tile " + tileIndex;

                tileArray[tileIndex].GetComponent<Tile>().gameBoardArrayIndex = tileIndex;

                LinkNeighbours(tileArray[tileIndex].GetComponent<Tile>());

                tileIndex++;
            }
        }
    }

    public void LinkNeighbours(Tile currentTile)
    {
        currentTile.neighbours.Clear();

        int index = currentTile.gameBoardArrayIndex;

        if (index == 0) // Top left corner - 2 neighbours
        {
            currentTile.neighbours.Add(index + 1);                    // right neighbour
            currentTile.neighbours.Add(index + boardWidth);           // bottom neighbour
        }
        else if (index == boardWidth - 1)    // Top right corner - 2 neighbours
        {
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index + boardWidth);         // bottom neighbour
        }
        else if ((index % boardWidth == 0 && index > ((boardLength * boardWidth - boardWidth - 1)))) // Bottom left corner
        {
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
            currentTile.neighbours.Add(index + 1);                  // right neighbour
        }
        else if (index == boardWidth * boardLength - 1)                                              // bottom right corner
        {
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
        }
        else if (index < boardWidth)                                                                 // Top edge
        {
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index + 1);                  // right neighbour
            currentTile.neighbours.Add(index + boardWidth);         // bottom neighbour
        }
        else if (index % boardWidth == 0)                                                            // Left Edge
        {
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
            currentTile.neighbours.Add(index + 1);                  // right neighbour
            currentTile.neighbours.Add(index + boardWidth);         // bottom neighbour
        }
        else if (index % boardWidth == boardWidth - 1)                                                 // Right Edge
        {
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index + boardWidth);         // bottom neighbour
        }
        else if (index > (((boardLength * boardWidth) - boardWidth) - 1))                            // Bottom Edge
        {
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
            currentTile.neighbours.Add(index + 1);                  // right neighbour
        }
        else                                                                                        // Central tile
        {
            currentTile.neighbours.Add(index - boardWidth);         // top neighbour
            currentTile.neighbours.Add(index - 1);                  // left neighbour
            currentTile.neighbours.Add(index + 1);                  // left neighbour
            currentTile.neighbours.Add(index + boardWidth);         // left neighbour
        }
    }

    public void PrepareSwapTiles(Tile firstTile, Tile secondTile)   // This function takes the two to swap from the player, once they're tested to be adjacent.
    {
        firstTileToSwap = firstTile;
        secondTileToSwap = secondTile;

        firstTypeDeadTiles.Clear();
        secondTypeDeadTiles.Clear();

        gameManagerScript.MakeMatch();
    }

    public void Swap()  // This function triggers the actual swap in the array
    {
        if (!firstTileToSwap)
        {
            return;
        }
        GameObject tempGameObject = firstTileToSwap.gameObject;

        int tempIndex1 = firstTileToSwap.gameBoardArrayIndex;
        int tempIndex2 = secondTileToSwap.gameBoardArrayIndex;

        firstTileToSwap.gameBoardArrayIndex = tempIndex2;
        secondTileToSwap.gameBoardArrayIndex = tempIndex1;

        tileArray[tempIndex1] = secondTileToSwap.gameObject;    // Make first equal to second
        tileArray[tempIndex2] = tempGameObject;                 // Make second equal to first (held by temp)

        tileArray[tempIndex1].name = "Tile " + tempIndex1;
        tileArray[tempIndex2].name = "Tile " + tempIndex2;

        LinkNeighbours(firstTileToSwap);
        LinkNeighbours(secondTileToSwap);

        if (firstTileToSwap.tileType == secondTileToSwap.tileType)
        {
            Debug.Log("TileTYPE " + firstTileToSwap.gameBoardArrayIndex + "   " + firstTileToSwap.tileType);
            firstTypeDeadTiles.Add(firstTileToSwap.gameBoardArrayIndex);
            firstTileToSwap.isDead = true;
            CheckNeighboursForSameTile(firstTileToSwap, firstTileToSwap.tileType, firstTypeDeadTiles);

            for (int i = 0; i < boardLength * boardWidth; ++i)
            {
                tileArray[i].GetComponent<Tile>().isChecked = false;
            }

            if (firstTypeDeadTiles.Count >= 3)
            {
                foreach (int index in firstTypeDeadTiles)
                {
                    Destroy(tileArray[index]);
                    tileArray[index] = null;
                }
                firstTypeDeadTiles.Clear();
            }
            else
            {
                Tile currentTile;
                foreach (int index in firstTypeDeadTiles)
                {
                    currentTile = tileArray[index].GetComponent<Tile>();
                    currentTile.isDead = false;
                    currentTile.isChecked = false;
                }
                firstTypeDeadTiles.Clear();
            }
        }
        else
        {
            /// First Type
            Debug.Log("TileTYPE " + firstTileToSwap.gameBoardArrayIndex + "   " + firstTileToSwap.tileType);
            firstTypeDeadTiles.Add(firstTileToSwap.gameBoardArrayIndex);
            firstTileToSwap.isDead = true;
            CheckNeighboursForSameTile(firstTileToSwap, firstTileToSwap.tileType, firstTypeDeadTiles);

            for(int i = 0; i < boardLength*boardWidth; ++i)
            {
                tileArray[i].GetComponent<Tile>().isChecked = false;
            }

            /// Second Type
            Debug.Log("TileTYPE " + secondTileToSwap.gameBoardArrayIndex + "   " + secondTileToSwap.tileType);
            secondTypeDeadTiles.Add(secondTileToSwap.gameBoardArrayIndex);
            secondTileToSwap.isDead = true;
            CheckNeighboursForSameTile(secondTileToSwap, secondTileToSwap.tileType, secondTypeDeadTiles);

            for (int i = 0; i < boardLength * boardWidth; ++i)
            {
                tileArray[i].GetComponent<Tile>().isChecked = false;
            }

            // First type destruction
            if (firstTypeDeadTiles.Count >= 3)
            {
                foreach (int index in firstTypeDeadTiles)
                {
                    Destroy(tileArray[index]);
                    tileArray[index] = null;
                }
                firstTypeDeadTiles.Clear();
            }
            else
            {
                Tile currentTile;
                foreach (int index in firstTypeDeadTiles)
                {
                    currentTile = tileArray[index].GetComponent<Tile>();
                    currentTile.isDead = false;
                    currentTile.isChecked = false;
                }
                firstTypeDeadTiles.Clear();
            }

            // Second type destruction
            if (secondTypeDeadTiles.Count >= 3)
            {
                foreach (int index in secondTypeDeadTiles)
                {
                    Destroy(tileArray[index]);
                    tileArray[index] = null;
                }
                secondTypeDeadTiles.Clear();
            }
            else
            {
                Tile currentTile;
                foreach (int index in secondTypeDeadTiles)
                {
                    currentTile = tileArray[index].GetComponent<Tile>();
                    currentTile.isDead = false;
                    currentTile.isChecked = false;
                }
                secondTypeDeadTiles.Clear();
            }
        }

        // Reset
        firstTileToSwap = null;
        secondTileToSwap = null;

        RepopulateBoard();
    }
    public bool TestAdjacent(Tile firstTile, Tile secondTile)
    {
        foreach (int neighbour in firstTile.neighbours)
        {
            if (secondTile.gameBoardArrayIndex == neighbour)
            {
                return true;
            }
        }

        return false;
    }

    public void CheckNeighboursForSameTile(Tile tileToCheck, Tile.TileType type, List<int> list)    // Recursive function that checks if: not already checked, and same tiletype. It does this for all neighbours of neighbours until done.
    {
        tileToCheck.isChecked = true;
        Debug.Log("-----Checking tile " + tileToCheck.gameBoardArrayIndex + "   " + type);
        foreach (int neighbour in tileToCheck.neighbours)
        {
            Tile currentTile = tileArray[neighbour].GetComponent<Tile>();
            if (currentTile.tileType == type && !currentTile.isChecked)
            {
                CheckNeighboursForSameTile(currentTile, type, list);
                list.Add(currentTile.gameBoardArrayIndex);
                currentTile.isDead = true;

                Debug.Log("   Killing Tile " + currentTile.gameBoardArrayIndex + "   " + currentTile.tileType);
            }
            else
            {
                Debug.Log(" Sparing Tile " + currentTile.gameBoardArrayIndex + "   " + currentTile.tileType);
                currentTile.isChecked = true;
                continue;
            }
        }
    }

    public void RepopulateBoard()
    {
        for(int i = 0; i < boardLength*boardWidth; ++i)
        {
            if(!tileArray[i])
            {
                int widthIndex = i % boardWidth;
                int lengthIndex = i / boardWidth;

                float x = widthIndex - ((boardWidth - 1.0f) / 2.0f);
                float z = lengthIndex - ((boardLength - 1.0f) / 2.0f);

                Vector3 position = new Vector3(x, 0.0f, z);
                tileArray[i] = Instantiate(tile, position, Quaternion.identity);

                tileArray[i].name = "Tile " + i;

                tileArray[i].GetComponent<Tile>().gameBoardArrayIndex = i;

                LinkNeighbours(tileArray[i].GetComponent<Tile>());
            }
        }
    }
}