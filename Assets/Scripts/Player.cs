﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    GameManager gameManagerScript;

    Board gameBoardScript;
    Camera cam;

    Tile firstTileSelected = null;
    Tile secondTileSelected = null;

    private void Awake()
    {
        gameManagerScript = FindObjectOfType<GameManager>();
        gameBoardScript = FindObjectOfType<Board>();
        cam = FindObjectOfType<Camera>();
    }

    public void SelectTile()
    {
        if (!gameManagerScript.isGameOver)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (!firstTileSelected && Physics.Raycast(ray, out hit))
            {
                firstTileSelected = hit.collider.GetComponent<Tile>();
                firstTileSelected.TileColour(Tile.TileType.Grey);

                Debug.Log("Tile: " + firstTileSelected.gameBoardArrayIndex + " selected.");
            }
            else if (Physics.Raycast(ray, out hit) && hit.collider.gameObject != firstTileSelected.gameObject)
            {
                secondTileSelected = hit.collider.GetComponent<Tile>();
                Debug.Log("Tile: " + secondTileSelected.gameBoardArrayIndex + " selected.");

                if (gameBoardScript.TestAdjacent(firstTileSelected, secondTileSelected))    // If they are neighbours in 1 of 4 directions, then they are allowed to swap.
                {
                    Debug.Log("They can be swapped!");

                    firstTileSelected.MoveTile(secondTileSelected.transform.position);
                    secondTileSelected.MoveTile(firstTileSelected.transform.position);

                    gameBoardScript.PrepareSwapTiles(firstTileSelected, secondTileSelected);

                    firstTileSelected = null;
                }
                else  // Reset everything
                {
                    firstTileSelected.TileColour(firstTileSelected.tileType);
                    firstTileSelected = null;

                    secondTileSelected.TileColour(secondTileSelected.tileType);
                    secondTileSelected = null;
                }
            }
            else if (firstTileSelected) // Reset everything
            {
                firstTileSelected.TileColour(firstTileSelected.tileType);
                firstTileSelected = null;
            }
        }
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SelectTile();
        }
    }
}