﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public GameManager gameManagerScript;

    public enum TileType
    {
        Red,
        Green,
        Blue,
        Yellow,
        Purple,
        Grey
    };

    public Material redMat;
    public Material blueMat;
    public Material greenMat;
    public Material yellowMat;
    public Material purpleMat;
    public Material greyMat;

    public TileType tileType;
    public int gameBoardArrayIndex;
    public List<int> neighbours;

    public Vector3 newPosition;
    public Vector3 oldPosition;
    public float moveSpeed;
    public bool isMoving = false;
    float lastFrameT = 0.0f;
    float lastFrameScaleT = 0.01f;

    public bool isDead = false;
    public bool isChecked = false;

    private void Awake()
    {
        gameManagerScript = FindObjectOfType<GameManager>();

        transform.localScale = Vector3.zero;
        lastFrameScaleT = 0.01f;

        tileType = (TileType)Random.Range(0, 5);
        TileColour(tileType);
        oldPosition = transform.position;

        isDead = false;
        isChecked = false;
        isMoving = false;
    }

    public void Update()
    {
        UpdateMovement();

        if (lastFrameScaleT < 1.0f)
        {
            float interpolateScale = lastFrameScaleT + (Time.deltaTime * moveSpeed);
            if (interpolateScale >= 1.0f) // If it has reached 100% LERP, we stop
            {
                transform.localScale = Vector3.one;
                interpolateScale = 0.0f;
            }
            else
            {
                transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, interpolateScale);
                lastFrameScaleT = interpolateScale;
            }
        }
    }

    public void MoveTile(Vector3 newPos)
    {
        // Get new position to lerp to
        newPosition = newPos;

        // new position, so movement should be updated
        isMoving = true;
        TileColour(tileType);   // reset tile colour to actual tiletype
    }

    void UpdateMovement()
    {
        if (isMoving)    // If its moving, make sure we update the t percentage for the LERP
        {
            float interpolate = lastFrameT + (Time.deltaTime * moveSpeed);
            if (interpolate >= 1.0f) // If it has reached 100% LERP, we reset for the next move, if any.
            {
                transform.position = newPosition;
                oldPosition = newPosition;
                isMoving = false;
                lastFrameT = 0.0f;
                interpolate = 0.0f;

                FindObjectOfType<Board>().Swap();   // Triggers the actual swap in the GameObject[]
            }
            else
            {
                transform.position = Vector3.Lerp(oldPosition, newPosition, interpolate);
                lastFrameT = interpolate;
            }
        }
    }

    public void TileColour(TileType matType)
    {
        switch (matType)
        {
            case TileType.Red:
                GetComponentInChildren<MeshRenderer>().material = redMat;
                break;
            case TileType.Green:
                GetComponentInChildren<MeshRenderer>().material = greenMat;
                break;
            case TileType.Blue:
                GetComponentInChildren<MeshRenderer>().material = blueMat;
                break;
            case TileType.Yellow:
                GetComponentInChildren<MeshRenderer>().material = yellowMat;
                break;
            case TileType.Purple:
                GetComponentInChildren<MeshRenderer>().material = purpleMat;
                break;
            case TileType.Grey:
                GetComponentInChildren<MeshRenderer>().material = greyMat;
                break;
        }
    }

    public void OnDestroy()
    {
        Debug.Log("DESTROYED. isChecked=" + isChecked + " : isDead=" + isDead + " : Type=" + tileType);
        gameManagerScript.Score();
    }
}